const app = angular.module( 'CartApp', [] )

app.controller( 'CartController', function( $scope ) {
	this.incart = []
	this.hascart = false
	this.modalStatus = ''
	this.completed = false
	this.carttotal = 0.00
	this.clear = function() {
		this.incart = []
		this.hascart = false
	}
	this.add = function( name, price ) {
		console.log('pushed!')
		this.incart.push( { 'name': name, 'price': price } )
		this.carttotal += parseFloat( price )
		this.hascart = true
	}
	this.openModal = function() {
		this.modalStatus = 'is-active'
	}
	this.closeModal = function() {
		this.modalStatus = ''
	}
	this.complete = function() {
		this.completed = true
	}
} )