<section class='hero is-primary has-text-centered'>

	<level class='level'>

		<div class='level-left'>

			<figure class='image is-64x64 logo'>
				<img src='grumpy.png' />
			</figure>

			<h3 class='title is-4'>
				CatMS</h3>

		</div>

		<div class='level-right'>

			<?php
			session_start();
				if(!isset($_SESSION['appuser']) && empty($_SESSION['appuser'])) {
					echo "
						<form method='post' action='indexlog.php' name='login'>
							<span>Login</span>
							<input type='text' name='user' value='Username'/>
							<input type='password' name='pass' value='Password'/>
							<button class='button is-small' type='submit'>Submit</button>
						</form>
					";
				} else {
					echo "
						<span>Welcome back, " . $_SESSION['appuser'] . "!</span><br>
						<form method='post' action='indexlog.php' name='logout'>
							<input type='hidden' name='logout' value='" . $_SESSION['appuser'] . "'/>
							<button type='submit' class='button'>Logout</button>
						</form>
					";
				}
			?>

		</div>

	</level>

	<div class='main-title'>

		<h1 class='title is-1'>
			CatMS</h1>

		<h3 class='subtitle is-4'>
			Cat SMS Subscription Service</h3>

	</div>

	<?php require('nav.php'); ?>

</section>