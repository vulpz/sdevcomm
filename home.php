<section class='section'>

	<div class='section-body'>

		<div class='columns'>

			<div class='column content has-text-centered'>

				<figure class='image left-image'>

					<img src='grumpy.png'/>

				</figure>

				<h2 class='is-3'>
					Having a catless day?</h2>

			</div>

			<div class='column content has-text-centered'>

				<h3 class='is-3'>
					That's down right sad.</h3>

				<p>Get a subscription today and never 
					be without cats a day in your life.</p>

				<div class='flashy-button'>

					<div class='flash-button-content'>

						<h2 class='is-2'>
							Subscribe</h2>

					</div>

				</div>

			</div>

		</div>

	</div>

</section>