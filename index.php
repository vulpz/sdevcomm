<!doctype html>
<html lang='en'>

	<!-- header -->
	<?php require('header.php'); ?>

	<!-- body -->
	<body>

		<!-- hero section (nav and banner) -->
		<?php require('hero.php'); ?>

		<!-- home "page" -->
		<?php require('home.php'); ?>

		<!-- footer -->
		<?php require('footer.php'); ?>

	</body>

</html>
