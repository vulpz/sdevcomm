<!doctype html>
<html lang='en'>

	<!-- header -->
	<?php require('header.php'); ?>

	<!-- body -->
	<body>

		<!-- hero section (nav and banner) -->
		<?php require('hero.php'); ?>

		<!-- subs "page" -->
		<?php require('subscriptions.php'); ?>

		<!-- footer -->
		<?php require('footer.php'); ?>

		<script type='text/javascript' src='cartcontroller.js'></script>

	</body>

</html>