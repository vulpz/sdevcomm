<section class='section'>

	<?php
		session_start();

		if(isset($_POST['user']) && !empty($_POST['user'])) {
			$_SESSION['appuser'] = $_POST['user'];
			$_SESSION['apppass'] = $_POST['pass'];
			header('refresh:3;url=index.php');
		}

		if(!isset($_SESSION['appuser']) && empty($_SESSION['appuser'])) {
			header('url=index.php');
		} else {
			echo "
				<span>Welcome back, !</span>
				<form method='post' action='indexlog.php' name='logout'>
					<input type='hidden' name='logout' value='" . $_SESSION['appuser'] . "'/>
					<button type='submit' class='button'>Logout</button>
				</form>
			";
		}

		if(isset($_POST['logout']) && !empty($_SESSION['appuser'])) {
			unset($_SESSION['appuser']);
			unset($_SESSION['apppass']);
			header('url=index.php');
		}
	?>