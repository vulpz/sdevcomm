<?php
	$contentarr = array(
		array(
			'title'=>'Freebie',
			'img'=>'free.png',
			'price'=>'Free!',
			'val'=>0.00
		),
		array(
			'title'=>'The Daily Fluff',
			'img'=>'199.png',
			'price'=>'$1.99/mo',
			'val'=>1.99
		),
		array(
			'title'=>'The Needed Kneading',
			'img'=>'299.png',
			'price'=>'$2.99/mo',
			'val'=>2.99
		),
		array(
			'title'=>'The Glued-to-Your-Phone',
			'img'=>'399.png',
			'price'=>'$3.99/mo',
			'val'=>3.99
		),
		array(
			'title'=>'The Mega Meow',
			'img'=>'499.png',
			'price'=>'$4.99/mo',
			'val'=>4.99
		),
		array(
			'title'=>'The Fur Will Not Stop',
			'img'=>'599.png',
			'price'=>'$5.99/mo',
			'val'=>5.99
		),
		array(
			'title'=>'The Cat Collector',
			'img'=>'699.png',
			'price'=>'$6.99/mo',
			'val'=>6.99
		),
		array(
			'title'=>'The Denial-ofanythingbut-Cats Attack',
			'img'=>'799.png',
			'price'=>'$7.99/mo',
			'val'=>7.99
		),
		array(
			'title'=>'The Fluffpocalypse',
			'img'=>'899.png',
			'price'=>'$8.99/mo',
			'val'=>8.99
		),
		array(
			'title'=>'The Whole Kitten Kaboodle',
			'img'=>'999.png',
			'price'=>'$9.99/mo',
			'val'=>9.99
		),
	);
?>

<section class='section' ng-app='CartApp' ng-controller='CartController as cart'>

	<div class='section-body'>

		<div class='floating button' ng-show='cart.hascart'>

			<button class='button' ng-click='cart.openModal()'>Checkout</button>

		</div>

		<?php
			$disabled = '';
			session_start();
				if(!isset($_SESSION['appuser']) && empty($_SESSION['appuser'])) {
					$disabled = '';
				} else {
					$disabled = 'ng-click=';
				}
			$objlen = count($contentarr);
			$rowlimit = 4;
			$rowcount = ceil($objlen / $rowlimit);

			for($i=0; $i<$rowcount; $i++) {

				echo "
					<div class='columns'>
				";

				for($j=($i * $rowlimit); $j<(($i * $rowlimit) + $rowlimit); $j++) {
					if(isset($contentarr[$j]) || array_key_exists($j, $contentarr)) {
						echo "
							<div class='column content has-text-centered is-one-fourth'>

								<div class='card'>

									<header class='card-header'>

										<p class='card-header-title'>
											" . $contentarr[$j]['title'] . "</p>

									</header>

									<div class='card-content'>

										<div class='content'>

											<figure class='image is-128x128 product-image'>

												<image src='" . $contentarr[$j]['img'] . "'/>

											</figure>

										</div>

									</div>

									<footer class='card-footer'>

										<p class='card-footer-item'>
											" . $contentarr[$j]['price'] . "</p>

										<a class='card-footer-item' " . $disabled . "'cart.add(\"" . $contentarr[$j]['title'] . "\", \"" . $contentarr[$j]['val'] . "\")'>
											Subscribe!</a>

									</footer>

								</div>

							</div>
						";
					}
				}

				echo "
					</div>
				";
			}
		?>
			
	</div>

	<div class="modal {{ cart.modalStatus }}">
  		
  		<div class="modal-background"></div>
  		
  		<div class="modal-content">
    		
  			<table class='table is-stripped is-bordered'>

  				<tr>

  					<th>Subscription</th>
  					<th>Price</th>

  				</tr>

  				<tr ng-repeat='item in cart.incart'>

  					<td>{{ item.name }}</td>
  					<td>{{ item.price }}</td>

  				</tr>

  				<tr>

  					<td><strong>Total:</strong></td>
  					<td><strong>{{ cart.carttotal }}</strong></td>

  				</tr>

  				<tr ng-hide='cart.completed'>

  					<td>Click here to complete order<br>
  						<em>Payments will be deducted from your DNAPal account</em></td>
  					<td><button class='button' ng-click='cart.complete()'>Confirm</button></td>

  				</tr>

  				<tr ng-show='cart.completed'>

  					<td>Congratz! Your subscription(s) have been initialized;
  						You should receive your first text, depending on plan, shortly.</td>

  				</tr>

  			</table>

  		</div>
  	
  		<button class="modal-close is-large" ng-click='cart.closeModal()'></button>

	</div>

</section>